package com.yanto.drawing.model;

import java.util.Arrays;
import java.util.Objects;

public class Canvas {
    private char[][] canvas;

    public void initialise(int width, int height) {
        if (width < 1 || height < 1) {
            System.err.println("Both width and height of the canvas must be positive numbers.");
            return;
        }

        canvas = new char[height][width];
        Arrays.stream(canvas).forEach(
            row -> Arrays.fill(row, ' ')
        );
    }

    public boolean isInitialised() {
        if (Objects.nonNull(canvas)) {
            return true;
        } else {
            System.err.println("Canvas has not been initialised. Please enter the command in the following format: 'C w h'.");
            return false;
        }
    }

    public boolean withinBounds(int x, int y, boolean fill) {
        if (x > 0 && y > 0 && x <= canvas[0].length && y <= canvas.length) {
            return true;
        } else {
            if (!fill) {
                System.err.println(String.format("(%d, %d) is outside the canvas.", x, y));
            }
            return false;
        }
    }

    public char getColour(int x, int y) {
        return canvas[y - 1][x - 1];
    }

    public void setColour(int x, int y, char c) {
        canvas[y - 1][x - 1] = c;
    }

    public void render() {
        if (Objects.isNull(canvas)) {
            return;
        }

        char[] borderTopBottom = new char[canvas[0].length + 2];
        Arrays.fill(borderTopBottom, '-');
        System.out.println(borderTopBottom);
        Arrays.stream(canvas).forEachOrdered(row -> {
            System.out.print('|');
            System.out.print(row);
            System.out.println('|');
        });
        System.out.println(borderTopBottom);
    }
}
