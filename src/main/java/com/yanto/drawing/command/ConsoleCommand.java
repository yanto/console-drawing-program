package com.yanto.drawing.command;

import com.yanto.drawing.model.Canvas;

public interface ConsoleCommand {

    void execute(Canvas canvas);
}
