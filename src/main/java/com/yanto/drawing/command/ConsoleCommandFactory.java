package com.yanto.drawing.command;

import com.yanto.drawing.command.impl.BucketFillCommand;
import com.yanto.drawing.command.impl.CanvasCommand;
import com.yanto.drawing.command.impl.LineCommand;
import com.yanto.drawing.command.impl.RectangleCommand;

import java.util.Arrays;

public class ConsoleCommandFactory {

    public static ConsoleCommand getCommand(String input) {
        String[] arr = input.trim().split(" ");
        String cmd = arr[0];
        String[] params = Arrays.copyOfRange(arr, 1, arr.length);

        switch(cmd) {
            case "C":
                return CanvasCommand.create(params);
            case "L":
                return LineCommand.create(params);
            case "R":
                return RectangleCommand.create(params);
            case "B":
                return BucketFillCommand.create(params);
            case "Q":
                System.err.println("To quit the program, please enter 'Q' only.");
                break;
            default:
                System.err.println("Unrecognised command. Accepted commands are 'C', 'L', 'R', 'B' and 'Q'.");
                break;
        }

        return null;
    }
}
