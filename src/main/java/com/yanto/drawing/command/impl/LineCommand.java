package com.yanto.drawing.command.impl;

import com.yanto.drawing.command.ConsoleCommand;
import com.yanto.drawing.model.Canvas;

import java.util.stream.IntStream;

public class LineCommand implements ConsoleCommand {

    private int x1;
    private int y1;
    private int x2;
    private int y2;

    public static LineCommand create(String... params) {
        if (params.length == 4) {
            try {
                int x1 = Integer.parseInt(params[0]);
                int y1 = Integer.parseInt(params[1]);
                int x2 = Integer.parseInt(params[2]);
                int y2 = Integer.parseInt(params[3]);

                return new LineCommand(x1, y1, x2, y2);
            } catch (NumberFormatException e) {
                System.err.println("Error creating a new line. Please enter the command in the following format: 'L x1 y1 x2 y2'.");
            }
        } else {
            System.err.println("Line command requires start and end coordinates as parameters. Please enter the command in the following format: 'L x1 y1 x2 y2'.");
        }
        return null;
    }

    public LineCommand(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    @Override
    public void execute(Canvas canvas) {
        if (!canvas.isInitialised() || !canvas.withinBounds(x1, y1, false) || !canvas.withinBounds(x2, y2, false)) {
            return;
        }

        if (x1 == x2) {
            int start = Math.min(y1, y2);
            int end = Math.max(y1, y2) + 1;
            IntStream.range(start, end).forEach(y -> canvas.setColour(x1, y, 'x'));
        } else if (y1 == y2) {
            int start = Math.min(x1, x2);
            int end = Math.max(x1, x2) + 1;
            IntStream.range(start, end).forEach(x -> canvas.setColour(x, y1, 'x'));
        } else {
            System.err.println("Currently only horizontal or vertical lines are supported.");
        }
    }
}
