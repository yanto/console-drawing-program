package com.yanto.drawing.command.impl;

import com.yanto.drawing.command.ConsoleCommand;
import com.yanto.drawing.model.Canvas;

public class RectangleCommand implements ConsoleCommand {

    private int x1;
    private int y1;
    private int x2;
    private int y2;

    public static RectangleCommand create(String... params) {
        if (params.length == 4) {
            try {
                int x1 = Integer.parseInt(params[0]);
                int y1 = Integer.parseInt(params[1]);
                int x2 = Integer.parseInt(params[2]);
                int y2 = Integer.parseInt(params[3]);

                return new RectangleCommand(x1, y1, x2, y2);
            } catch (NumberFormatException e) {
                System.err.println("Error creating a new rectangle. Please enter the command in the following format: 'R x1 y1 x2 y2'.");
            }
        } else {
            System.err.println("Rectangle command requires start and end coordinates as parameters. Please enter the command in the following format: 'R x1 y1 x2 y2'.");
        }
        return null;
    }

    public RectangleCommand(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public void execute(Canvas canvas) {
        if (!canvas.isInitialised() || !canvas.withinBounds(x1, y1, false) || !canvas.withinBounds(x2, y2, false)) {
            return;
        }

        if (x1 == x2) {
            System.err.println("x1 is equal to x2. This will draw a vertical line instead.");
        } else if (y1 == y2) {
            System.err.println("y1 is equal to y2. This will draw a horizontal line instead.");
        } else {
            new LineCommand(x1, y1, x2, y1).execute(canvas);
            new LineCommand(x1, y2, x2, y2).execute(canvas);
            new LineCommand(x1, y1, x1, y2).execute(canvas);
            new LineCommand(x2, y1, x2, y2).execute(canvas);
        }
    }
}
