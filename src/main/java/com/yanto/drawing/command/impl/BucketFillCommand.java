package com.yanto.drawing.command.impl;

import com.yanto.drawing.command.ConsoleCommand;
import com.yanto.drawing.model.Canvas;

public class BucketFillCommand implements ConsoleCommand {

    private int x;
    private int y;
    private char c;

    public static BucketFillCommand create(String... params) {
        if (params.length == 3) {
            try {
                int x = Integer.parseInt(params[0]);
                int y = Integer.parseInt(params[1]);
                char c = params[2].charAt(0);

                return new BucketFillCommand(x, y, c);
            } catch (NumberFormatException e) {
                System.err.println("Error filling the area with colour c. Please enter the command in the following format: 'B x y c'.");
            }
        } else {
            System.err.println("Bucket fill command requires a coordinate and a colour as parameters. Please enter the command in the following format: 'B x y c'.");
        }
        return null;
    }

    public BucketFillCommand(int x, int y, char c) {
        this.x = x;
        this.y = y;
        this.c = c;
    }

    public void execute(Canvas canvas) {
        if (!canvas.isInitialised() || !canvas.withinBounds(this.x, this.y, false)) {
            return;
        }

        char toBeReplaced = canvas.getColour(x, y);
        fill(canvas, this.x, this.y, toBeReplaced);
    }

    private void fill(Canvas canvas, int x, int y, char toBeReplaced) {
        if (canvas.withinBounds(x, y, true)) {
            char currentColour = canvas.getColour(x, y);
            if (currentColour == toBeReplaced) {
                canvas.setColour(x, y, this.c);

                fill(canvas, x + 1, y, toBeReplaced);
                fill(canvas, x - 1, y, toBeReplaced);
                fill(canvas, x, y + 1, toBeReplaced);
                fill(canvas, x, y - 1, toBeReplaced);
            }
        }
    }
}
