package com.yanto.drawing.command.impl;

import com.yanto.drawing.command.ConsoleCommand;
import com.yanto.drawing.model.Canvas;

public class CanvasCommand implements ConsoleCommand {

    private int width;
    private int height;

    public static CanvasCommand create(String... params) {
        if (params.length == 2) {
            try {
                int width = Integer.parseInt(params[0]);
                int height = Integer.parseInt(params[1]);

                return new CanvasCommand(width, height);
            } catch (NumberFormatException e) {
                System.err.println("Error creating a new canvas. Please enter the command in the following format: 'C w h'.");
            }
        } else {
            System.err.println("Canvas command requires width and height as parameters. Please enter the command in the following format: 'C w h'.");
        }
        return null;
    }

    public CanvasCommand(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void execute(Canvas canvas) {
        canvas.initialise(width, height);
    }
}
