package com.yanto.drawing;

import com.yanto.drawing.command.ConsoleCommand;
import com.yanto.drawing.command.ConsoleCommandFactory;
import com.yanto.drawing.model.Canvas;

import java.io.Console;
import java.util.Objects;

public class App {

    public static void main(String[] args) {
        Console console = System.console();
        if (Objects.isNull(console)) {
            System.out.println("Non-interactive environment detected! Exiting...");
            System.exit(0);
        }

        Canvas canvas = new Canvas();
        String input;
        boolean quitApp = false;

        while(!quitApp) {
            System.out.println();
            System.out.print("enter command: ");
            input = console.readLine();
            System.out.println();

            if (("Q").equals(input.trim())) {
                quitApp = true;
            } else {
                ConsoleCommand command = ConsoleCommandFactory.getCommand(input);
                if (Objects.nonNull(command)) {
                    command.execute(canvas);
                    canvas.render();
                }
            }
        }
    }
}
