package com.yanto.drawing.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CanvasTest {

    @Test
    public void testInitialiseFailed() {
        Canvas canvas = new Canvas();

        canvas.initialise(0, 1);
        assertThat(canvas.isInitialised()).isFalse();
    }

    @Test
    public void testInitialiseSuccess() {
        Canvas canvas = new Canvas();

        canvas.initialise(1, 1);
        assertThat(canvas.isInitialised()).isTrue();
    }

    @Test
    public void testWithinBoundsXNegative() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.withinBounds(-1, 1, false)).isFalse();
    }

    @Test
    public void testWithinBoundsXZero() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.withinBounds(0, 1, false)).isFalse();
    }

    @Test
    public void testWithinBoundsXOver() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.withinBounds(25, 4, false)).isFalse();
    }

    @Test
    public void testWithinBoundsYNegative() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.withinBounds(1, -1, false)).isFalse();
    }

    @Test
    public void testWithinBoundsYZero() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.withinBounds(1, 0, false)).isFalse();
    }

    @Test
    public void testWithinBoundsYOver() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.withinBounds(10, 5, false)).isFalse();
    }

    @Test
    public void testWithinBounds() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.withinBounds(3, 3, false)).isTrue();
    }

    @Test
    public void testGetColour() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.getColour(3, 3)).isEqualTo(' ');
    }

    @Test
    public void testSetColour() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        canvas.setColour(3, 3, 'x');
        assertThat(canvas.getColour(3, 3)).isEqualTo('x');
    }
}
