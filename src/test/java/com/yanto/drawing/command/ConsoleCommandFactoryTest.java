package com.yanto.drawing.command;

import com.yanto.drawing.command.impl.BucketFillCommand;
import com.yanto.drawing.command.impl.CanvasCommand;
import com.yanto.drawing.command.impl.LineCommand;
import com.yanto.drawing.command.impl.RectangleCommand;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConsoleCommandFactoryTest {

    @Test
    public void testGetCanvasCommand() {
        assertThat(ConsoleCommandFactory.getCommand("C 20 4")).isInstanceOf(CanvasCommand.class);
    }

    @Test
    public void testGetLineCommand() {
        assertThat(ConsoleCommandFactory.getCommand("L 1 2 6 2")).isInstanceOf(LineCommand.class);
    }

    @Test
    public void testGetRectangleCommand() {
        assertThat(ConsoleCommandFactory.getCommand("R 14 1 18 3")).isInstanceOf(RectangleCommand.class);
    }

    @Test
    public void testGetBucketFillCommand() {
        assertThat(ConsoleCommandFactory.getCommand("B 10 3 c")).isInstanceOf(BucketFillCommand.class);
    }

    @Test
    public void testQuitCommand() {
        assertThat(ConsoleCommandFactory.getCommand("Q a")).isNull();
    }

    @Test
    public void testUnrecognisedCommandSingleChar() {
        assertThat(ConsoleCommandFactory.getCommand("X")).isNull();
    }

    @Test
    public void testUnrecognisedCommandMultipleChars() {
        assertThat(ConsoleCommandFactory.getCommand("XYZ")).isNull();
    }

    @Test
    public void testUnrecognisedCommandWithParams() {
        assertThat(ConsoleCommandFactory.getCommand("X 1 2 3")).isNull();
    }

    @Test
    public void testWhitespace() {
        assertThat(ConsoleCommandFactory.getCommand(" C 20 4   ")).isInstanceOf(CanvasCommand.class);
    }
}
