package com.yanto.drawing.command.impl;

import com.yanto.drawing.model.Canvas;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LineCommandTest {

    @Test
    public void testCreateLineCommandInsufficientParams() {
        String[] params = {"1", "2"};
        assertThat(LineCommand.create(params)).isNull();
    }

    @Test
    public void testCreateLineCommandTooManyParams() {
        String[] params = {"1", "2", "3", "4", "5"};
        assertThat(LineCommand.create(params)).isNull();
    }

    @Test
    public void testCreateLineCommandInvalidParams() {
        String[] params = {"a", "2", "3", "4"};
        assertThat(LineCommand.create(params)).isNull();
    }

    @Test
    public void testCreateLineCommandSuccess() {
        String[] params = {"1", "2", "3", "4"};
        assertThat(LineCommand.create(params)).isInstanceOf(LineCommand.class);
    }

    @Test
    public void testExecuteDiagonalLine() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.isInitialised()).isTrue();

        LineCommand command = new LineCommand(1, 2, 3, 4);
        command.execute(canvas);
        assertThat(canvas.getColour(1, 2)).isEqualTo(' ');
        assertThat(canvas.getColour(3, 4)).isEqualTo(' ');
    }

    @Test
    public void testExecuteHorizontalLine() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.isInitialised()).isTrue();

        LineCommand command = new LineCommand(1, 2, 6, 2);
        command.execute(canvas);
        assertThat(canvas.getColour(3, 2)).isEqualTo('x');
        assertThat(canvas.getColour(3, 3)).isEqualTo(' ');
    }

    @Test
    public void testExecuteVerticalLine() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.isInitialised()).isTrue();

        LineCommand command = new LineCommand(6, 2, 6, 4);
        command.execute(canvas);
        assertThat(canvas.getColour(6, 2)).isEqualTo('x');
        assertThat(canvas.getColour(3, 3)).isEqualTo(' ');
    }
}
