package com.yanto.drawing.command.impl;

import com.yanto.drawing.model.Canvas;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CanvasCommandTest {

    @Test
    public void testCreateCanvasCommandInsufficientParams() {
        String[] params = {"20"};
        assertThat(CanvasCommand.create(params)).isNull();
    }

    @Test
    public void testCreateCanvasCommandTooManyParams() {
        String[] params = {"20", "1", "1"};
        assertThat(CanvasCommand.create(params)).isNull();
    }

    @Test
    public void testCreateCanvasCommandInvalidParams() {
        String[] params = {"a", "a"};
        assertThat(CanvasCommand.create(params)).isNull();
    }

    @Test
    public void testCreateCanvasCommandSuccess() {
        String[] params = {"20", "4"};
        assertThat(CanvasCommand.create(params)).isInstanceOf(CanvasCommand.class);
    }

    @Test
    public void testExecute() {
        Canvas canvas = new Canvas();
        CanvasCommand command = new CanvasCommand(20, 4);
        command.execute(canvas);
        assertThat(canvas.isInitialised()).isTrue();
    }
}
