package com.yanto.drawing.command.impl;

import com.yanto.drawing.model.Canvas;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RectangleCommandTest {

    @Test
    public void testCreateRectangleCommandInsufficientParams() {
        String[] params = {"1", "2"};
        assertThat(RectangleCommand.create(params)).isNull();
    }

    @Test
    public void testCreateRectangleCommandTooManyParams() {
        String[] params = {"1", "2", "3", "4", "5"};
        assertThat(RectangleCommand.create(params)).isNull();
    }

    @Test
    public void testCreateRectangleCommandInvalidParams() {
        String[] params = {"a", "2", "3", "4"};
        assertThat(RectangleCommand.create(params)).isNull();
    }

    @Test
    public void testCreateRectangleCommandSuccess() {
        String[] params = {"1", "2", "3", "4"};
        assertThat(RectangleCommand.create(params)).isInstanceOf(RectangleCommand.class);
    }

    @Test
    public void testExecuteHorizontalLine() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.isInitialised()).isTrue();

        RectangleCommand command = new RectangleCommand(14, 1, 18, 1);
        command.execute(canvas);
        assertThat(canvas.getColour(18, 1)).isEqualTo(' ');
        assertThat(canvas.getColour(15, 2)).isEqualTo(' ');
    }

    @Test
    public void testExecuteVerticalLine() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.isInitialised()).isTrue();

        RectangleCommand command = new RectangleCommand(14, 1, 14, 3);
        command.execute(canvas);
        assertThat(canvas.getColour(18, 1)).isEqualTo(' ');
        assertThat(canvas.getColour(15, 2)).isEqualTo(' ');
    }

    @Test
    public void testExecuteSuccess() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.isInitialised()).isTrue();

        RectangleCommand command = new RectangleCommand(14, 1, 18, 3);
        command.execute(canvas);
        assertThat(canvas.getColour(18, 1)).isEqualTo('x');
        assertThat(canvas.getColour(15, 2)).isEqualTo(' ');
    }
}
