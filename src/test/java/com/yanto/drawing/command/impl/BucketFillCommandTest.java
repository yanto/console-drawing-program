package com.yanto.drawing.command.impl;

import com.yanto.drawing.model.Canvas;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BucketFillCommandTest {

    @Test
    public void testCreateBucketFillCommandInsufficientParams() {
        String[] params = {"1", "2"};
        assertThat(BucketFillCommand.create(params)).isNull();
    }

    @Test
    public void testCreateBucketFillCommandTooManyParams() {
        String[] params = {"1", "2", "a", "b"};
        assertThat(BucketFillCommand.create(params)).isNull();
    }

    @Test
    public void testCreateBucketFillCommandInvalidParams() {
        String[] params = {"a", "2", "b"};
        assertThat(BucketFillCommand.create(params)).isNull();
    }

    @Test
    public void testCreateBucketFillCommandSuccess() {
        String[] params = {"10", "3", "o"};
        assertThat(BucketFillCommand.create(params)).isInstanceOf(BucketFillCommand.class);
    }

    @Test
    public void testExecute() {
        Canvas canvas = new Canvas();

        canvas.initialise(20, 4);
        assertThat(canvas.isInitialised()).isTrue();

        // Draw horizontal line
        canvas.setColour(1, 2, 'x');
        canvas.setColour(2, 2, 'x');
        canvas.setColour(3, 2, 'x');

        // Draw vertical line
        canvas.setColour(10, 1, 'a');
        canvas.setColour(10, 2, 'a');
        canvas.setColour(10, 3, 'a');

        // Fill horizontal line with colour 'o'. Everything else should remain the same.
        BucketFillCommand command = new BucketFillCommand(2, 2, 'o');
        command.execute(canvas);
        assertThat(canvas.getColour(1, 2)).isEqualTo('o');
        assertThat(canvas.getColour(10, 3)).isEqualTo('a');
        assertThat(canvas.getColour(5, 4)).isEqualTo(' ');
    }
}
