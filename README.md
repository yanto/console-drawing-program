## Console Drawing Program

01. This project contains the source code for the console drawing program, written in the Java programming language. The project is generated using the [Gradle](http://gradle.org) build tool. To ensure compatibility, please use Java 8 and Gradle 4.6 when building and running this program.

02. The source code of the console drawing program can be found under **/src/main/java**. The files are structured as follows:

        com.yanto.drawing
          |
          -- App.java
          |
          -- model
          |    |
          |    -- Canvas.java
          |
          -- command
               |
               -- ConsoleCommand.java
               -- ConsoleCommandFactory.java
               |
               -- impl
                   |
                   -- CanvasCommand.java
                   -- LineCommand.java
                   -- RectangleCommand.java
                   -- BucketFillCommand.java

03. Test cases are written in [JUnit](https://junit.org/junit4/) and [AssertJ](http://joel-costigliola.github.io/assertj/). They can be found under **/src/test/java** and have similar structure as the source code for the drawing program above.

04. To build the program, open a command line terminal and change to the root directory of the project (console-drawing-program). The command to build the program is:

        ./gradlew clean build

05. The result of the build is an executable Java JAR file, which can be found under **/build/lib**. The file name is labelled **console-drawing-program-1.0.jar**.

06. To run the program, ensure that the Java 8 runtime is installed. The command to run the program is:

        java -jar build/libs/console-drawing-program-1.0.jar
